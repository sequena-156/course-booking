import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';
import './App.css';

//acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import LoginPage from './pages/Login';
import Catalog from './pages/Courses';
import ErrorPage from './pages/Error';
import CourseView from './pages/CourseView';
import Logout from './pages/Logout';
import AddCourse from './pages/AddCourse';
import UpdateCourse from './pages/UpdateCourse'

//acquire the provider utility in this entry point
import {UserProvider} from './UserContext';

//implement page routing in our app
//acquire the utilities from react-router-dom
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
  //BrowserRouter -> this is a standard library component for routing in react. this enables navigation amongst views of various components. It will serve as the 'parent' component that will be used to store all the other components.
  //as -> alias keyword in JS.

  //Routes => it's a new component introduced in V6 of react-router-dom whose task is to allow switching between locations.

//JSX Component -> self closing tag
//syntax: <Element />
function App() {

  //the role of the Provider was assign to the App.js, which means all the information that we will declared here will automatically becomes 'global' scope.
  //initialize a state of the user to provide/identify the status of client who is using the app.

  //id => to reference the user.
  //isADMIN => role of the user
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  }) //to bind the user to the default state.

  //Create a function that will unset/unmount the user 
  const unsetUser = () =>{
    //clear out the saved data in the local storage
    //remove all data in the local storage.
    localStorage.clear(); //remove the token.
    //revert the state of the user back to null.
    setUser({
      id: null,
      isAdmin: null
    });
  }

  //feed the information stated here to the consumers.
  useEffect(()=>{
    //identify the state of the user
    // console.log(user);
    //mount/campaign the user to the app so that he/she will be recognized, using the token.
    //retrieve the token from the browser storage.
    let token = localStorage.getItem('accessToken');
    // console.log(token)

    //fetch('url', 'options'), keep in mind, in this request we are passing down a token.
    fetch('https://morning-earth-29301.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json())
    .then(convertedData => {
      // console.log(convertedData)
      //identify the procedures that will happen if the info about the user is retrieved successfully.
      //change the current state of the user.
      if (typeof convertedData._id !== "undefined") {
          setUser({
            id: convertedData._id,
            isAdmin: convertedData.isAdmin
          });
          // console.log(user);
      } else {
        //if the condition above is not met
         setUser({
            id: null,
            isAdmin: null
          });
      }
    });

  },[user]);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavBar />
          <Routes>
            <Route path='/' element={<Home/>} />
            <Route path='/register' element={<Register/>}  />
            <Route path='/courses' element={<Catalog />} />
            <Route path='/login' element={<LoginPage />} />
            <Route path='/courses/view/:id' element={<CourseView />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/courses/add' element={<AddCourse />} />
            <Route path='/courses/update' element={<UpdateCourse />} />
            <Route path='*' element={<ErrorPage />} />
          </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
