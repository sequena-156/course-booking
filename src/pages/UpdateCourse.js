//components needed to create AddCourse

import {Container, Form, InputGroup, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function UpdateCourse() {

	const createCourse = (event) => {
		event.preventDefault()
		return(
			//display a message that will confirm to the user that registration is successful.
			Swal.fire({
				icon: 'success',
				title: 'Successfully Updated Course',
			})
		);
};
	return (
		<div>
			

			<Container>
				<h1 className="text-center mt-5">Update Course</h1>
				<Form onSubmit={e=>createCourse(e)} >
					{/*Course Name Field*/}
					<Form.Group>
						<Form.Label>Course Name:</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Name" required/>
					</Form.Group>
					{/*Course Description Field*/}
					<Form.Group>
						<Form.Label>Course Description:</Form.Label>
						<Form.Control type="text" placeholder="Enter Course Description" required/>
					</Form.Group>
				
					{/*Price Field*/}
						<Form.Label>Price:</Form.Label>
					<InputGroup>
						<InputGroup.Text>PHP</InputGroup.Text>
						<Form.Control type="number" placeholder="Enter Price" required/>
					</InputGroup>
					{/*Add Course Button*/}
					<Form>
					  <Form.Check 
					    type="switch"
					    id="custom-switch"
					    label="Disable Course"
					  />
					</Form>
					<Button type="submit" className="btn-info mb-3 my-3">Update Course</Button>
				</Form>
			</Container>



		</div>



		);
	};

